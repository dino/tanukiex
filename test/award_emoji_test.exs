defmodule Tanukiex.AwardEmojiTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock
  import Tanukiex.AwardEmoji
  require IEx

  doctest Tanukiex.AwardEmoji

  @credentials Tanukiex.credentials("https://gitlab.com/api/v3/", "ABC1234567")

  test "list award emoji by issue" do
    use_cassette "award_emoji#list_by_issue" do
      response = @credentials |> list_by_issue(11111, 22222)
      assert response.status_code == 200
      assert List.first(response.body).id == 1234
    end
  end

  test "list award emoji by merge request" do
    use_cassette "award_emoji#list_by_merge_request" do
      response = @credentials |> list_by_merge_request(11111, 222222)
      assert response.status_code == 200
      assert List.first(response.body).id == 4567
    end
  end

  test "get single award emoji from issue" do
    use_cassette "award_emoji#get_single_from_issue" do
      response = @credentials |> get_single_from_issue(1111, 2222, 3333)
      assert response.status_code == 200
      assert response.body.awardable_id == 2222
    end
  end

  test "get single award emoji from merge request" do
    use_cassette "award_emoji#get_single_from_merge_request" do
      response = @credentials |> get_single_from_merge_request(1111, 2222, 3333)
      assert response.status_code == 200
      assert response.body.id == 51898
    end
  end

  test "awards an award emoji to an issue" do
    use_cassette "award_emoji#award_emoji_to_issue" do
      response = @credentials |> award_emoji_to_issue(111, 222, "bow_and_arrow")
      assert response.status_code == 201
      assert response.body.name == "bow_and_arrow"
    end
  end

  test "deletes an award emoji from an issue" do
    use_cassette "award_emoji#delete_award_emoji_from_issue" do
      response = @credentials |> delete_award_emoji_from_issue(1111, 2222, 3333)
      assert response.status_code == 200
      assert response.body.id == 3333
    end
  end

  test "deletes an award emoji from a merge request" do
    use_cassette "award_emoji#delete_award_emoji_from_merge_request" do
      response = @credentials |> delete_award_emoji_from_merge_request(1111, 2222, 3333)
      assert response.status_code == 200
      assert response.body.id == 3333
    end
  end

  test "list award emoji from note" do
    use_cassette "award_emoji#list_from_note" do
      response = @credentials |> list_from_note(1111, 2222, 3333)
      assert response.status_code == 200
      assert List.first(response.body).name == "football"
    end
  end

  test "get single award emoji from note" do
    use_cassette "award_emoji#get_single_from_note" do
      response = @credentials |> get_single_from_note(1111, 2222, 3333, 4444)
      assert response.status_code == 200
      assert response.body.id == 4444
    end
  end

  test "awards an award emoji to a note" do
    use_cassette "award_emoji#award_emoji_to_note" do
      response = @credentials |> award_emoji_to_note(1111, 2222, 3333, "football")
      assert response.status_code == 201
      assert response.body.name == "football"
    end
  end

  test "deletes an award emoji from a note" do
    use_cassette "award_emoji#delete_award_emoji_from_note" do
      response = @credentials |> delete_award_emoji_from_note(1111, 2222, 3333, 4444)
      assert response.status_code == 200
      assert response.body.id == 4444
    end
  end
end
