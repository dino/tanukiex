defmodule Tanukiex.BuildTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock
  import Tanukiex.Build
  require IEx

  doctest Tanukiex.Build

  @credentials Tanukiex.credentials("https://gitlab.com/api/v3/", "Qa6fVZ4BFxENwtJrqVco")

  test "list builds by project" do
    use_cassette "build#list_by_project" do
      response = @credentials |> list_by_project(1111)
      assert response.status_code == 200
      assert List.first(response.body).stage == "test"
    end
  end

  test "list commit builds by project" do
    use_cassette "build#list_commit_builds_by_project" do
      response = @credentials |> list_commits_by_project(1111, "94a19tr544c93e233e2662832967ad3e0cdd4f9f")
      assert response.status_code == 200
      assert List.first(response.body).status == "success"
    end
  end

  test "get a single build of a project" do
    use_cassette "build#single_by_project" do
      response = @credentials |> single_by_project(1111, 2222)
      assert response.status_code == 200
      assert response.body.status == "success"
    end
  end

  test "cancel a single build of a project" do
    use_cassette "build#cancel" do
      response = @credentials |> cancel(1130437, 4338039)
      assert response.status_code == 201
      assert response.body.status == "success"
    end
  end

  test "retry a single build of a project" do
    use_cassette "build#retry" do
      response = @credentials |> retry(1130437, 4338039)
      assert response.status_code == 201
      assert response.body.status == "pending"
    end
  end

  test "erase a single build of a project" do
    use_cassette "build#erase" do
      response = @credentials |> erase(1130437, 4338039)
      assert response.status_code == 201
      assert response.body.status == "success"
    end
  end

  test "prevents artifacts from being deleted when expiration is set." do
    use_cassette "build#keep_artifacts" do
      response = @credentials |> keep_artifacts(1130437, 4756641)
      assert response.status_code == 200
      assert response.body.status == "success"
    end
  end

  test "triggers a manual action to start a build." do
    use_cassette "build#play" do
      response = @credentials |> keep_artifacts(1130437, 4756641)
      assert response.status_code == 200
      assert response.body.status == "success"
    end
  end
end
