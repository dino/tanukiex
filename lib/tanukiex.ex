defmodule Tanukiex do
  alias Tanukiex.Credentials
  require IEx
  @moduledoc """
    Tanukiex core module acts as a middleware 
    handling all communication using HTTPotion and Poison.
  """

  def credentials(private_token), do: %Credentials{private_token: private_token}
  def credentials(endpoint, private_token) do
    %Credentials{endpoint: endpoint, private_token: private_token}
  end

  def get(%{endpoint: endpoint, private_token: private_token}, path, body \\ %{}) do
    endpoint
    |> url(path)
    |> Tanukiex.Request.get([body: body, headers: ["PRIVATE-TOKEN": private_token]])
    |> process_response
  end

  def post(%{endpoint: endpoint, private_token: private_token}, path, body \\ %{}) do
    endpoint
    |> url(path)
    |> Tanukiex.Request.post([body: body, headers: ["PRIVATE-TOKEN": private_token]])
    |> process_response
  end

  def put(%{endpoint: endpoint, private_token: private_token}, path, body \\ %{}) do
    endpoint
    |> url(path)
    |> Tanukiex.Request.put([body: body, headers: ["PRIVATE-TOKEN": private_token]])
    |> process_response
  end

  def delete(%{endpoint: endpoint, private_token: private_token}, path, body \\ %{}) do
    endpoint
    |> url(path)
    |> Tanukiex.Request.delete([body: body, headers: ["PRIVATE-TOKEN": private_token]])
    |> process_response
  end

  @spec url(String.t, String.t) :: String.t
  defp url(endpoint, path) do
    String.replace_trailing(endpoint, "/", "") <> path
  end

  @spec process_response(HTTPotion.Response.t) :: Tanukiex.Response
  defp process_response(%{status_code: status_code, body: body}) do
    %Tanukiex.Response{status_code: status_code, body: body}
  end

  @spec process_response(HTTPotion.Response.ErrorResponse) :: Tanukiex.Response
  defp process_response(%{message: message}) do
    %Tanukiex.Response{status_code: 408, body: message}
  end
end
