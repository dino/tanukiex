defmodule Tanukiex.Response do
  @moduledoc """
    Tanukiex.Response module is responsible for encapsulating
    responses from GitLab.
  """

  @typedoc """
  Response struct is used to build the response map using 
  the `status_code` and `body` fields.
  """
  @type t :: %__MODULE__{status_code: integer, body: any}

  @doc """
  Build a response struct.  

  ```
    %Tanukiex.Response{status_code: 200, body: ["id": 345,
                                                "name": "rocket",
                                                "user": {
                                                  "name": "Administrator",
                                                  ...
                                                },
                                                ...
                                                "awardable_id": 1,
                                                "awardable_type": "Note"]}
  ```
  """
  defstruct status_code: nil, body: nil
end
