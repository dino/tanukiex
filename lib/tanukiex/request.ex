defmodule Tanukiex.Request do
  use HTTPotion.Base
  import Tanukiex.Serializer

  @moduledoc """
    Tanukiex.Request module is a wrapper for HTTPotion requests
  """

  def process_request_headers(headers) do
    Dict.put headers, :"User-Agent", "tanukiex"
    Dict.put headers, :"Content-Type", "application/json"
  end

  def process_request_body(body), do: encode(body)

  def process_response_body(body), do: decode(body)
end
