defmodule Tanukiex.Commits.Action do
  @create "create"
  @delete "delete"
  @move   "move"
  @update "update"

  @moduledoc """
    Tanukiex.Commits.Action module is responsible for encapsulating
    a commmit action.
  """

  @typedoc """
  Action struct is used to build an action map.  
  """
  @type t :: %__MODULE__{
                          action: String.t,
                          file_path: String.t,
                          previous_path: String.t,
                          content: String.t,
                          encoding: String.t
                        }

  @doc """
  Build a commit actions struct.
    * `action` - The action to perform, create, delete, move, update
    * `file_path` - Full path to the file. Ex. lib/class.rb
    * `previous_path` - Original full path to the file being moved. Ex. lib/class1.rb
    * `content` - File content, required for all except delete. Optional for move
    * `encoding` - text or base64. text is default.

    ```
    %Tanukiex.Commits.Action.create("lib/class.rb", "lib/class1.rb", "blah", "base64")
    %Tanukiex.Commits.Action.delete("lib/class.rb", "lib/class1.rb", "blah", "base64")
    %Tanukiex.Commits.Action.move("lib/class.rb", "lib/class1.rb", "blah", "base64")
    %Tanukiex.Commits.Action.update("lib/class.rb", "lib/class1.rb", "blah", "base64")
    ```
  """

  defstruct [:action, :file_path, :previous_path, :content, :encoding]

  @spec create(String.t, String.t, String.t, String.t) :: %__MODULE__{}
  def create(file_path, previous_path, content, encoding) do
    action(@create, file_path, previous_path, content, encoding)
  end
  
  @spec delete(String.t, String.t, String.t, String.t) :: %__MODULE__{}
  def delete(file_path, previous_path, content, encoding) do
    action(@delete, file_path, previous_path, content, encoding)
  end

  @spec move(String.t, String.t, String.t, String.t) :: %__MODULE__{}
  def move(file_path, previous_path, content, encoding) do
    action(@move, file_path, previous_path, content, encoding)
  end
  
  @spec update(String.t, String.t, String.t, String.t) :: %__MODULE__{}
  def update(file_path, previous_path, content, encoding) do
    action(@update, file_path, previous_path, content, encoding)
  end
  
  @spec action(String.t, String.t, String.t, String.t, String.t) :: %__MODULE__{}
  defp action(action_type, file_path, previous_path, content, encoding) do
    %__MODULE__{
                 action: action_type,
                 file_path: file_path,
                 previous_path: previous_path,
                 content: content,
                 encoding: encoding
               }
  end
end
