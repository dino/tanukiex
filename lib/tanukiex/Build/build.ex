defmodule Tanukiex.Build do
  import Tanukiex

  @moduledoc """
  This module maps all build api resources to a matching function.

  *** API Reference: *** http://docs.gitlab.com/ce/api/builds.html  
  """

  @doc """
  Get a list of builds in a project.    

  *** API Reference: *** 
  http://docs.gitlab.com/ce/api/builds.html#list-project-builds  
  
    ```
    Tanukiex.Build.list_by_project(credentials, 12345)
    ```
  """
  @spec list_by_project(Credentials.t, integer) :: Tanukiex.Response
  def list_by_project(credentials, project_id) do
    get(credentials, "/projects/#{project_id}/builds")
  end

  @doc """
  Get a list of commit builds in a project.    

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#list-commit-builds  
  
    ```
    Tanukiex.Build.list_commits_by_project(credentials, 12345, 94a19tr544c93e233e2662832967ad3e0cdd4f9f)
    ```
  """
  @spec list_commits_by_project(Credentials.t, integer, String.t) :: Tanukiex.Response
  def list_commits_by_project(credentials, project_id, sha) do
    get(credentials, "/projects/#{project_id}/repository/commits/#{sha}/builds")
  end

  @doc """
  Get a single build of a project.    

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#get-a-single-build
  
    ```
    Tanukiex.Build.single_by_project(credentials, 12345, 6789)
    ```
  """
  @spec single_by_project(Credentials.t, integer, integer) :: Tanukiex.Response
  def single_by_project(credentials, project_id, build_id) do
    get(credentials, "/projects/#{project_id}/builds/#{build_id}")
  end

  @doc """
  Cancel a single build of a project

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#cancel-a-build
  
    ```
    Tanukiex.Build.cancel(credentials, 12345, 6789)
    ```
  """
  @spec cancel(Credentials.t, integer, integer) :: Tanukiex.Response
  def cancel(credentials, project_id, build_id) do
    post(credentials, "/projects/#{project_id}/builds/#{build_id}/cancel")
  end

  @doc """
  Retry a single build of a project

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#retry-a-build
  
    ```
    Tanukiex.Build.retry(credentials, 12345, 6789)
    ```
  """
  @spec retry(Credentials.t, integer, integer) :: Tanukiex.Response
  def retry(credentials, project_id, build_id) do
    post(credentials, "/projects/#{project_id}/builds/#{build_id}/retry")
  end

  @doc """
  Erase a build

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#erase-a-build
  
    ```
    Tanukiex.Build.erase(credentials, 12345, 6789)
    ```
  """
  @spec erase(Credentials.t, integer, integer) :: Tanukiex.Response
  def erase(credentials, project_id, build_id) do
    post(credentials, "/projects/#{project_id}/builds/#{build_id}/erase")
  end

  @doc """
  Keep artifacts

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#keep-artifacts
  
    ```
    Tanukiex.Build.keep_artifacts(credentials, 12345, 6789)
    ```
  """
  @spec keep_artifacts(Credentials.t, integer, integer) :: Tanukiex.Response
  def keep_artifacts(credentials, project_id, build_id) do
    post(credentials, "/projects/#{project_id}/builds/#{build_id}/artifacts/keep")
  end

  @doc """
  Triggers a manual action to start a build.

  *** API Reference: *** 
  https://docs.gitlab.com/ce/api/builds.html#play-a-build
  
    ```
    Tanukiex.Build.play(credentials, 12345, 6789)
    ```
  """
  @spec play(Credentials.t, integer, integer) :: Tanukiex.Response
  def play(credentials, project_id, build_id) do
    post(credentials, "/projects/#{project_id}/builds/#{build_id}/play")
  end
end