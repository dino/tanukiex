# Tanukiex

Tanukiex is an awesome love-powered Elixir GitLab API wrapper.

Disclaimer: It's still in a very early stage, only a few resource endpoints are covered.

## Examples

#### 1) Setup credentials

Default endpoint:
 
```elixir
iex> credentials = Tanukiex.credentials("BB8C3P0")
%Tanukiex.Credentials{endpoint: "https://gitlab.com/api/v3/", private_token: "BB8C3P0"}
```

Using your own GitLab Instance:
 
```elixir
iex> credentials = Tanukiex.credentials("https://gitlab.example.com/api/v3/", "BB8C3P0")
%Tanukiex.Credentials{endpoint: "https://gitlab.example/api/v3/", private_token: "BB8C3P0"}
```

#### 2) Doing stuff


Getting a list of awarded emojis of an issue

```elixir
iex> Tanukiex.AwardEmoji.list_by_issue(credentials, 13579, 223547)
%Tanukiex.Response{body: [%{awardable_id: 2792185, awardable_type: "Issue",
    created_at: "2016-08-19T16:52:58.020Z", id: 50818, name: "100",
    updated_at: "2016-08-19T16:52:58.020Z",
    user: %{avatar_url: "https://secure.gravatar.com/avatar/57b990f3c277b0730266f956ebd13692?s=80&d=identicon",
      id: 5871, name: "Marcelo Lebre", state: "active",
      username: "marcelo.lebre",
      web_url: "https://gitlab.com/u/marcelo.lebre"}},
  }], status_code: 200}  
```

Response


## Tests
 * ``` mix test ``` - to run test suite.
 * ``` mix dogma ``` - to run code style linter
 * ``` mix inch ``` - to run doc style analyzer

## Documentation

Run ```./generate_docs.sh``` to generate documentation.

## Covered Resources  

- [X] Award Emoji
  * [X]  List an awardable's award emoji
  * [X]  Get single award emoji
  * [X]  Award a new emoji
  * [X]  Delete an award emoji
  * [X]  List a note's award emoji
  * [X]  Get single note's award emoji
  * [X]  Award a new emoji on a note
  * [X]  Delete an award emoji

- [X] Branches
  * [X]  List repository branches
  * [X]  Get single repository branch
  * [X]  Protect repository branch
  * [X]  Unprotect repository branch
  * [X]  Create repository branch
  * [X]  Delete repository branch

- [X] Builds
  * [X] List project builds
  * [X] List commit builds
  * [X] Get a single build
  * [ ] Get build artifacts
  * [ ] Download the artifacts file
  * [ ] Get a trace file
  * [X] Cancel a build
  * [X] Retry a build
  * [X] Erase a build
  * [X] Keep artifacts
  * [X] Play a build

- [X] Build triggers
  * [X] List project triggers
  * [X] Get trigger details
  * [X] Create a project trigger
  * [X] Remove a project trigger

- [X] Build Variables
  * [X] List project variables
  * [X] Show variable details
  * [X] Create variable
  * [X] Update variable
  * [X] Remove variable

- [X] Commits
  * [X] List repository commits
  * [X] Create a commit with multiple files and actions
  * [X] Get a single commit
  * [X] Get the diff of a commit
  * [X] Get the comments of a commit
  * [X] Post comment to commit
  * [X] Get the status of a commit
  * [X] Post the build status to a commit

- [ ] Deploy Keys
- [ ] Groups
- [ ] Group Access Requests
- [ ] Group Members
- [ ] Issues
- [ ] Keys
- [ ] Labels
- [ ] Merge Requests
- [ ] Milestones
- [ ] Open source license templates
- [ ] Namespaces
- [ ] Notes (comments)
- [ ] Pipelines
- [ ] Projects including setting Webhooks
- [ ] Project Access Requests
- [ ] Project Members
- [ ] Project Snippets
- [ ] Repositories
- [ ] Repository Files
- [ ] Runners
- [ ] Services
- [ ] Session
- [ ] Settings
- [ ] Sidekiq metrics
- [ ] System Hooks
- [ ] Tags
- [ ] Users
- [ ] Todos
